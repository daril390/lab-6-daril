from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps

from .views import booksearch

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class myProfileUnitTest(TestCase):

    #URL DAN VIEWS
    def test_bookSearch_url_is_exist(self):
        response = Client().get('/bookSearch/')
        self.assertEqual(response.status_code, 200)
    
    def test_myProfile_using_index_template(self):
        response = Client().get('/bookSearch/')
        self.assertTemplateUsed(response, 'booksearch/index.html')

    def test_myProfile_using_index_func(self):
        found = resolve('/bookSearch/')
        self.assertEqual(found.func, booksearch)

class functional_test(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(functional_test, self).setUp()


    def tearDown(self):
        self.browser.quit()
        super(functional_test, self).tearDown()

    def test_input(self):
        self.browser.get('http://127.0.0.1:8000/bookSearch/')
        self.assertInHTML("Search Your Favourites Books", self.browser.page_source)
        search = self.browser.find_element_by_id('search')
        submit = self.browser.find_element_by_id('button')
        search.send_keys("test")
        submit.send_keys(Keys.RETURN)
        
