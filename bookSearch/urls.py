from django.urls import path
from . import views


app_name = 'bookSearch'

urlpatterns = [
    path('', views.booksearch, name="booksearch"),
]