$(document).ready(function(){
    $.ajax({
        method: "GET",
        url: "https://www.googleapis.com/books/v1/volumes?q="+ "anime",
        success: function(data){
            $("#results").empty();
            $("#results").append(
                "<thead>" +
                    "<tr class = 'text-center'>" +
                        "<th scope='col'>Cover</th>" + 
                        "<th scope='col'>Title</th>" +
                        "<th scope='col'>Author</th>" +
                        "<th scope='col'>Publisher</th>" +
                    "</tr>" +
                "</thead>"
            );
            for(i = 0; i < data.items.length; i++){
                if(data.items[i].volumeInfo.imageLinks != null){
                    $("#results").append(
                        "<tbody>" + 
                            "<tr class = 'text-center'>" +
                                "<td><img src = '" + data.items[i].volumeInfo.imageLinks.thumbnail + "'></td>" + 
                                "<td>" + data.items[i].volumeInfo.title + "</td>" +
                                "<td>" + data.items[i].volumeInfo.author + "</td>" +
                                "<td>" + data.items[i].volumeInfo.publisher + "</td>" +
                            "</tr>" +
                        "</tbody>"
                    );
                }
                else{
                    $("#results").append(
                        "<tbody>" + 
                            "<tr class = 'text-center'>" +
                                "<td></td>" + 
                                "<td>" + data.items[i].volumeInfo.title + "</td>" +
                                "<td>" + data.items[i].volumeInfo.author + "</td>" +
                                "<td>" + data.items[i].volumeInfo.publisher + "</td>" +
                            "</tr>" +
                        "</tbody>"
                    );
                }
            }
        }
    });
    $("#button").click(function(){
        console.log($('#search').val())
        $.ajax({
            method: "GET",
            url: "https://www.googleapis.com/books/v1/volumes?q="+$('#search').val(),
            datatype: "json",

            success: function(data){
                $("#results").empty();
                $("#results").append(
                    "<thead>" +
                        "<tr class = 'text-center'>" +
                            "<th scope='col'>Cover</th>" + 
                            "<th scope='col'>Title</th>" +
                            "<th scope='col'>Author</th>" +
                            "<th scope='col'>Publisher</th>" +
                        "</tr>" +
                    "</thead>"
                );
                for(i = 0; i < data.items.length; i++){
                    if(data.items[i].volumeInfo.imageLinks != null){
                        $("#results").append(
                            "<tbody>" + 
                                "<tr class = 'text-center'>" +
                                    "<td><img src = '" + data.items[i].volumeInfo.imageLinks.thumbnail + "'></td>" + 
                                    "<td>" + data.items[i].volumeInfo.title + "</td>" +
                                    "<td>" + data.items[i].volumeInfo.author + "</td>" +
                                    "<td>" + data.items[i].volumeInfo.publisher + "</td>" +
                                "</tr>" +
                            "</tbody>"
                        );
                    }
                    else{
                        $("#results").append(
                            "<tbody>" + 
                                "<tr class = 'text-center'>" +
                                    "<td></td>" + 
                                    "<td>" + data.items[i].volumeInfo.title + "</td>" +
                                    "<td>" + data.items[i].volumeInfo.author + "</td>" +
                                    "<td>" + data.items[i].volumeInfo.publisher + "</td>" +
                                "</tr>" +
                            "</tbody>"
                        );
                    }
                }
            },
        });
    });
});