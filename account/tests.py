from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps

from .views import index
from .views import register
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.forms import UserCreationForm

# Create your tests here.

class AccountUnitTest(TestCase):
    
    #URL DAN VIEWS
    def test_account_url_is_exist(self):
        response = Client().get('/account/')
        self.assertEqual(response.status_code, 200)
    
    def test_account_using_index_template(self):
        response = Client().get('/account/')
        self.assertTemplateUsed(response, 'account/index.html')
    
    def test_account_using_index_func(self):
        found = resolve('/account/')
        self.assertEqual(found.func, index)

    def test_register_url_is_exist(self):
        response = Client().get('/account/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_account_using_index_template(self):
        response = Client().get('/account/register/')
        self.assertTemplateUsed(response, 'registration/register.html')
    
    def test_account_using_index_func(self):
        found = resolve('/account/register/')
        self.assertEqual(found.func, register)
    
    def test_login_url_is_exist(self):
        response = Client().get('/account/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_account_using_index_template(self):
        response = Client().get('/account/login/')
        self.assertTemplateUsed(response, 'registration/login.html')
    
    def test_logout_url_is_exist(self):
        response = Client().get('/account/logout/')
        self.assertEqual(response.status_code, 302)
    
    def test_form(self):
        form_data = {
            'username' : 'test',
            'password1' : 'testing390',
            'password2' : 'testing390',
        }
        form = UserCreationForm(data = form_data)
        self.assertTrue(form.is_valid())
        request = Client().post('/account/register/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/account/login/')
        self.assertEqual(response.status_code, 200)

