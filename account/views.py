from django.shortcuts import render, redirect

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required

from django.http import HttpResponse
from django.contrib.auth import login, authenticate, logout

# Create your views here.


def index(request):
    return render(request, 'account/index.html')

def register(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/account/login/')
    else:
        form = UserCreationForm()
    context = {
        'form' : form,
    }
    return render(request, 'registration/register.html', context)