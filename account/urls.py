from django.urls import path
from . import views

from django.contrib.auth.views import LoginView, LogoutView

app_name = 'account'

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.register, name="register"),
    path('login/', LoginView.as_view(), name="login"),
    path('logout/', LogoutView.as_view(next_page="/account/login/"), name="logout"),
]