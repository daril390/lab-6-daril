from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps
from homepage.apps import HomepageConfig

from .models import response
from .views import index
from .forms import ResponseForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class homepageUnitTest(TestCase):

    #URL DAN VIEWS
    def test_homepage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_homepage_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage/index.html')

    def test_homepage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    #MODELS
    def create_response(self):
        example_response = response.objects.create(status = "Test")
        return example_response
    
    def test_check_response(self):
        test = self.create_response()
        self.assertTrue(isinstance(test, response))
        self.assertEqual(response.objects.all().count(), 1)

    #APPS
    def test_apps(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')

    #FORMS
    def test_form(self):
        form_data = {
            'status' : 'Test isi form',
        }
        form = ResponseForm(data = form_data)
        self.assertTrue(form.is_valid())
        request = Client().post('/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

class functional_test(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(functional_test, self).setUp()


    def tearDown(self):
        self.browser.quit()
        super(functional_test, self).tearDown()

    def test_input_todo(self):
        self.browser.get('http://127.0.0.1:8000')
        status = self.browser.find_element_by_id('id_status')
        submit = self.browser.find_element_by_id('submit')
        status.send_keys("Coba Coba")
        submit.send_keys(Keys.RETURN)