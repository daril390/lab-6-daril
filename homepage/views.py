from django.shortcuts import render, redirect
from .forms import ResponseForm
from .models import response

# Create your views here.

def index(request):
    responses = response.objects.all()
    if request.method == "POST":
        form = ResponseForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect ('/')
    
    else:
        form = ResponseForm()
    context = {
        'form' : form,
        'respon' : responses,
    }

    return render(request, 'homepage/index.html', context)