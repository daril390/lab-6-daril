$(document).ready(function(){
    $( "#accordion" ).accordion({
      collapsible: true,
      active: false
    });
    $("#switch").change(function(){
        if ($('#switch').is(':checked')){
            trans()
            $("html").attr('data-theme', 'dark')
            $("nav").removeClass("navbar sticky-top navbar-expand-lg navbar-light bg-light")
            $("table").removeClass("table table-striped table-light mt-3")
            $("nav").addClass("navbar sticky-top navbar-expand-lg navbar-dark bg-dark")
            $("table").addClass("table table-striped table-dark mt-3")
        }else{
            trans()
            $("html").attr('data-theme', 'light')
            $("nav").removeClass("navbar sticky-top navbar-expand-lg navbar-dark bg-dark")
            $("table").removeClass("table table-striped table-dark mt-3")
            $("nav").addClass("navbar sticky-top navbar-expand-lg navbar-light bg-light")
            $("table").addClass("table table-striped table-light mt-3")
        }
    })
  let trans = () => {
      document.documentElement.classList.add('transition');
      window.setTimeout(() => {
          document.documentElement.classList.remove('transition')
      }, 1000)
  }
})