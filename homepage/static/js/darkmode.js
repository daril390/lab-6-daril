$(document).ready(function(){
    $("#switch").change(function(){
        if ($('#switch').is(':checked')){
            trans()
            $("html").attr('data-theme', 'dark')
        }else{
            trans()
            $("html").attr('data-theme', 'light')
        }
    })
  let trans = () => {
      document.documentElement.classList.add('transition');
      window.setTimeout(() => {
          document.documentElement.classList.remove('transition')
      }, 1000)
  }
})