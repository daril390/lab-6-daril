from django import forms

from .models import response

class ResponseForm(forms.ModelForm):
    class Meta:
        model = response
        fields = [
            'status',
        ]