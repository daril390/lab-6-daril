from django.urls import path
from . import views


app_name = 'myProfile'

urlpatterns = [
    path('', views.profile, name="profile"),
]