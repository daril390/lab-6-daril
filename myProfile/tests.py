from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps

from myProfile.views import profile

# Create your tests here.

class myProfileUnitTest(TestCase):

    #URL DAN VIEWS
    def test_myProfile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)
    
    def test_myProfile_using_index_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'myProfile/index.html')

    def test_myProfile_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)